package com.nbarthelemy.paiementbdm.api.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;

@Entity // This tells Hibernate to make a table out of this class
@Table(name = "eventplayers")
@Getter
@Setter
@NoArgsConstructor
@ToString
public class EventPlayer implements Cloneable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private int id;
	@Column(name="eventid")
	private int eventId;
	@Column(name="playerid")
	private int playerId;
	@Column(name="payment")
	private float payment;

	public EventPlayer(int eventId, int playerId, float payment) {
		super();
		this.eventId = eventId;
		this.playerId = playerId;
		this.payment = payment;
	}

	@Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
