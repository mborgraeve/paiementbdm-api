package com.nbarthelemy.paiementbdm.api.services;

import com.nbarthelemy.paiementbdm.api.entities.EventPlayer;
import com.nbarthelemy.paiementbdm.api.repository.EventPlayerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class EventPlayerService {
	
	@Autowired
	private EventPlayerRepository eventPlayerRepository;
	
	@Autowired
	public EventPlayerService() {

	}

	/**
	 * Get all the EventPlayer
	 * @return All the EventPlayer
	 */
	public Iterable<EventPlayer> getAll() {
		return eventPlayerRepository.findAll();
	}

	/**
	 * Get a EventPlayer by Id
	 * @param id The EventPlayer Id
	 * @return The EventPlayer if found, else null
	 */
	public EventPlayer get(int id) {
		return eventPlayerRepository.findById(id).orElse(null);
	}

	/**
	 * Get a EventPlayer by EventId
	 * @param eventId The Event Id
	 * @return A list of EventPlayers
	 */
	public List<EventPlayer> getByEvent(int eventId) {
		List<EventPlayer> eventPlayerList = ((List<EventPlayer>)getAll());

		return eventPlayerList.stream()
				.filter(ep -> ep.getEventId() == eventId)
				.collect(Collectors.toList());
	}

	/**
	 * Get a EventPlayer by PlayerId
	 * @param playerId The Player Id
	 * @return A list of EventPlayers
	 */
	public List<EventPlayer> getByPlayer(int playerId) {
		List<EventPlayer> eventPlayerList = ((List<EventPlayer>)getAll());

		return eventPlayerList.stream()
				.filter(ep -> ep.getPlayerId() == playerId)
				.collect(Collectors.toList());
	}


	/**
	 * Get EventPlayer by EventId and PlayerId
	 * @param eventId The eventId
	 * @param playerId The playerId
	 * @return The retrieved EventPlayer or null
	 */
	public EventPlayer get(int eventId, int playerId) {
		List<EventPlayer> eventPlayerList = ((List<EventPlayer>)getAll());

		return eventPlayerList.stream()
				.filter(p -> p.getEventId() == eventId && p.getPlayerId() == playerId)
				.findAny().orElse(null);
	}

	/***
	 * Create the EventPlayer passed in parameter if it does not exist
	 * Additional existence criteria: same eventId and playerId
	 * @param eventPlayer The EventPlayer to be created
	 * @return The created EventPlayer
	 * @throws Exception if the EventPlayer already exists
	 */
	public EventPlayer create(EventPlayer eventPlayer) throws Exception {
		if (get(eventPlayer.getId()) != null) {
			throw new Exception("EventPlayer with Id '" + eventPlayer.getId() + "' already exists ! Can't create.");
		}

		// check eventId and playerId
		EventPlayer checkedEventPlayer = get(eventPlayer.getEventId(), eventPlayer.getPlayerId());
		if (checkedEventPlayer != null) {
			throw new Exception("An EventPlayer (Id: " + checkedEventPlayer.getId() + ") with eventId '"
					+ eventPlayer.getEventId() + "' and playerId '" + eventPlayer.getPlayerId()
					+ " already exists ! Can't create.");
		}

		return eventPlayerRepository.save(eventPlayer);
	}

	/**
	 * Update the EventPlayer passed in parameter if it exists.
	 * If no Id has been passed, we throw an exception.
	 * @param eventPlayer The EventPlayer to be updated
	 * @return The updated EventPlayer
	 * @throws Exception if the EventPlayer does not exists
	 */
	public EventPlayer update(EventPlayer eventPlayer) throws Exception {
		if (get(eventPlayer.getId()) == null) {
			throw new Exception("EventPlayer with id: '" + eventPlayer.getId() + "' does not exist ! Can't update.");
		}

		EventPlayer checkedEventPlayer = get(eventPlayer.getEventId(), eventPlayer.getPlayerId());
		if (checkedEventPlayer == null) {
			throw new Exception("No EventPlayer with eventId '" + eventPlayer.getEventId()
					+ "' and playerId '" + eventPlayer.getPlayerId() + " exist ! Can't update.");
		}

		return eventPlayerRepository.save(eventPlayer);
	}

	/**
	 * Delete the EventPlayer by id
	 * @param id The id
	 */
	public void delete(int id) {
		eventPlayerRepository.deleteById(id);
	}
	
//	private Event getTestEvent() {
//		return new Event(
//				9,
//				"BDM FC A",
//				"5",
//				"2",
//				"Paris Lutece",
//				new Date(System.currentTimeMillis()),
//				7,
//				"Foot a 7",
//				"1"
//			);
//	}

}