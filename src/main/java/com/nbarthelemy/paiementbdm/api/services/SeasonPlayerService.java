package com.nbarthelemy.paiementbdm.api.services;

import com.nbarthelemy.paiementbdm.api.entities.SeasonPlayer;
import com.nbarthelemy.paiementbdm.api.repository.SeasonPlayerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class SeasonPlayerService {

	@Autowired
	private SeasonPlayerRepository seasonPlayerRepository;

	@Autowired
	public SeasonPlayerService() {

	}

	/**
	 * Get all the SeasonPlayer
	 * @return All the SeasonPlayer
	 */
	public Iterable<SeasonPlayer> getAll() {
		return seasonPlayerRepository.findAll();
	}

	/**
	 * Get a SeasonPlayer by Id
	 * @param id The SeasonPlayer Id
	 * @return The SeasonPlayer if found, else null
	 */
	public SeasonPlayer get(int id) {
		return seasonPlayerRepository.findById(id).orElse(null);
	}

	/**
	 * Get a SeasonPlayer by SeasonId
	 * @param seasonId The Season Id
	 * @return A list of SeasonPlayer
	 */
	public List<SeasonPlayer> getBySeason(int seasonId) {
		List<SeasonPlayer> seasonPlayerList = ((List<SeasonPlayer>)getAll());

		return seasonPlayerList.stream()
				.filter(ep -> ep.getSeasonId() == seasonId)
				.collect(Collectors.toList());
	}

	/**
	 * Get a SeasonPlayer by PlayerId
	 * @param playerId The Player Id
	 * @return A list of SeasonPlayer
	 */
	public List<SeasonPlayer> getByPlayer(int playerId) {
		List<SeasonPlayer> seasonPlayerList = ((List<SeasonPlayer>)getAll());

		return seasonPlayerList.stream()
				.filter(ep -> ep.getPlayerId() == playerId)
				.collect(Collectors.toList());
	}


	/**
	 * Get SeasonPlayer by SeasonId and PlayerId
	 * @param seasonId The seasonId
	 * @param playerId The playerId
	 * @return The retrieved SeasonPlayer or null
	 */
	public SeasonPlayer get(int seasonId, int playerId) {
		List<SeasonPlayer> seasonPlayerList = ((List<SeasonPlayer>)getAll());

		return seasonPlayerList.stream()
				.filter(p -> p.getSeasonId() == seasonId && p.getPlayerId() == playerId)
				.findAny().orElse(null);
	}

	/***
	 * Create the SeasonPlayer passed in parameter if it does not exist
	 * Additional existence criteria: same seasonId and playerId
	 * @param seasonPlayer The SeasonPlayer to be created
	 * @return The created SeasonPlayer
	 * @throws Exception if the SeasonPlayer already exists
	 */
	public SeasonPlayer create(SeasonPlayer seasonPlayer) throws Exception {
		if (get(seasonPlayer.getId()) != null) {
			throw new Exception("SeasonPlayer with Id '" + seasonPlayer.getId() + "' already exists ! Can't create.");
		}

		// check seasonId and playerId
		SeasonPlayer checkedSeasonPlayer = get(seasonPlayer.getSeasonId(), seasonPlayer.getPlayerId());
		if (checkedSeasonPlayer != null) {
			throw new Exception("An SeasonPlayer (Id: " + checkedSeasonPlayer.getId() + ") with seasonId '"
					+ seasonPlayer.getSeasonId() + "' and playerId '" + seasonPlayer.getPlayerId()
					+ " already exists ! Can't create.");
		}

		return seasonPlayerRepository.save(seasonPlayer);
	}

	/**
	 * Update the SeasonPlayer passed in parameter if it exists.
	 * If no Id has been passed, we throw an exception.
	 * @param seasonPlayer The SeasonPlayer to be updated
	 * @return The updated SeasonPlayer
	 * @throws Exception if the SeasonPlayer does not exists
	 */
	public SeasonPlayer update(SeasonPlayer seasonPlayer) throws Exception {
		if (get(seasonPlayer.getId()) == null) {
			throw new Exception("SeasonPlayer with id: '" + seasonPlayer.getId() + "' does not exist ! Can't update.");
		}

		SeasonPlayer checkedSeasonPlayer = get(seasonPlayer.getSeasonId(), seasonPlayer.getPlayerId());
		if (checkedSeasonPlayer == null) {
			throw new Exception("No SeasonPlayer with seasonId '" + seasonPlayer.getSeasonId()
					+ "' and playerId '" + seasonPlayer.getPlayerId() + " exist ! Can't update.");
		}

		return seasonPlayerRepository.save(seasonPlayer);
	}

	/**
	 * Delete the SeasonPlayer by id
	 * @param id The id
	 */
	public void delete(int id) {
		seasonPlayerRepository.deleteById(id);
	}
	
//	private Event getTestEvent() {
//		return new Event(
//				9,
//				"BDM FC A",
//				"5",
//				"2",
//				"Paris Lutece",
//				new Date(System.currentTimeMillis()),
//				7,
//				"Foot a 7",
//				"1"
//			);
//	}

}