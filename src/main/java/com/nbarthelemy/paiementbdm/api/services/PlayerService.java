package com.nbarthelemy.paiementbdm.api.services;

import com.nbarthelemy.paiementbdm.api.entities.Player;
import com.nbarthelemy.paiementbdm.api.entities.SeasonPlayer;
import com.nbarthelemy.paiementbdm.api.repository.PlayerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class PlayerService {
	
	@Autowired
	private PlayerRepository playerRepository;

	@Autowired
	private SeasonPlayerService seasonPlayerService;

	@Autowired
	public PlayerService() {

	}

	/**
	 * Get all the Player
	 * @return All the Player
	 */
	public Iterable<Player> getAll() {
		return playerRepository.findAll();
	}

	/**
	 * Get all the Player by Season
	 * @return All the Player by Season
	 */
	public Iterable<Player> getBySeason(int id) {
		List<SeasonPlayer> seasonPlayers = seasonPlayerService.getBySeason(id);
		return ((List<Player>)getAll()).stream()
				.filter(p -> seasonPlayers.stream()
						.anyMatch(sp -> sp.getPlayerId() == p.getId())
				)
				.collect(Collectors.toList());
	}

	/**
	 * Get a Player by Id
	 * @param id The Player Id
	 * @return The Player if found, else null
	 */
	public Player get(int id) {
		return playerRepository.findById(id).orElse(null);
	}

	/***
	 * Create the Player passed in parameter if it does not exist
	 * Additional existence criteria: same bdmId
	 * @param player The Player to be created
	 * @return The created Player
	 * @throws Exception if the Player already exists (checked by bdmId)
	 */
	public Player create(Player player) throws Exception {
		Player existingPlayer = get(player.getId());

		if (existingPlayer != null) {
			throw new Exception("Player with Id '" + player.getId() + "' already exists ! Can't create.");
		}

		// check bdmId
		List<Player> playerList = ((List<Player>)getAll());
		Player playerBDMId = playerList.stream()
				.filter(p -> p.getBdmId() == player.getBdmId())
				.findAny().orElse(null);
		if (playerBDMId != null) {
			throw new Exception("A Player (Id: " + playerBDMId.getId() + ") with bdmId '" + player.getBdmId()
					+ "' already exists ! Can't create.");
		}

		return playerRepository.save(player);
	}

	/**
	 * Update the Player passed in parameter if it exists.
	 * If no Id has been passed, we check if the bdmId already exists and update that Player if it's the case.
	 * @param player The Player to be updated
	 * @return The updated Player
	 * @throws Exception if the Player does not exists (checked by bdmId)
	 */
	public Player update(Player player) throws Exception {
		if (get(player.getId()) == null) {
			throw new Exception("Player with id: '" + player.getId() + "' does not exist ! Can't update.");
		}

		return playerRepository.save(player);
	}

	/**
	 * Delete the Player by id
	 * @param id The id
	 */
	public void delete(int id) {
		playerRepository.deleteById(id);
	}

//	public boolean deleteById() {
//		playerRepository.deleteById();
//	}
	
//	private Player getTestPlayer() {
//		return new Player(
//				9,
//				"BDM FC A",
//				"5",
//				"2",
//				"Paris Lutece",
//				new Date(System.currentTimeMillis()),
//				7,
//				"Foot a 7",
//				"1"
//			);
//	}

}