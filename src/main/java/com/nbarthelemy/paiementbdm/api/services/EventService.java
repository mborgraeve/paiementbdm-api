package com.nbarthelemy.paiementbdm.api.services;

import com.nbarthelemy.paiementbdm.api.dto.SubmitEvent;
import com.nbarthelemy.paiementbdm.api.dto.SubmitEventResult;
import com.nbarthelemy.paiementbdm.api.entities.Event;
import com.nbarthelemy.paiementbdm.api.entities.EventPlayer;
import com.nbarthelemy.paiementbdm.api.repository.EventRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class EventService {

	/*private List<Event> events;*/
	
	@Autowired
	private EventRepository eventRepository;

	@Autowired
	private EventPlayerService eventPlayerService;

	@Autowired
	private PlayerService playerService;

	@Autowired
	public EventService() {

	}

	/**
	 * Get all the Event
	 * @return All the Event
	 */
	public Iterable<Event> getAll() {
		return eventRepository.findAll();
	}


	/**
	 * Get all the Event by Season
	 * @return All the Event by Season
	 */
	public Iterable<Event> getBySeason(int id) {
		return ((List<Event>)getAll()).stream()
				.filter(ev -> ev.getSeasonId() == id
				)
				.collect(Collectors.toList());
	}

	/**
	 * Get a Event by Id
	 * @param id The Event Id
	 * @return The Event if found, else null
	 */
	public Event get(int id) {
		return eventRepository.findById(id).orElse(null);
	}

	/***
	 * Create the Event passed in parameter if it does not exist
	 * @param event The Event to be created
	 * @return The created Event
	 * @throws Exception if the Event already exists
	 */
	public Event create(Event event) throws Exception {
		Event existingEvent = get(event.getId());

		if (existingEvent != null) {
			throw new Exception("Event with Id '" + event.getId() + "' already exists ! Can't create.");
		}

		return eventRepository.save(event);
	}

	/***
	 * Create the list of Event passed in parameter if they do not exist
	 * @param events The list of Event to be created
	 * @return The list of the created Event
	 * @throws Exception if an Event already exists
	 */
	public Iterable<Event> bulkCreate(List<Event> events) throws Exception {
		for (Event event : events) {
			Event existingEvent = get(event.getId());
			if (existingEvent != null) {
				throw new Exception("Event with Id '" + event.getId() + "' already exists ! Can't create.");
			}
		}

		return eventRepository.saveAll(events);
	}

	/**
	 * Update the Event passed in parameter if it exists
	 * @param event The Event to be updated
	 * @return The updated Event
	 * @throws Exception if the Event does not exists
	 */
	public Event update(Event event) throws Exception {
		if (get(event.getId()) == null) {
			throw new Exception("Event with id: '" + event.getId() + "' does not exist ! Can't update.");
		}

		return eventRepository.save(event);
	}

	/**
	 * Submit the Event
	 * ! We ignore the id that are passed !
	 * We will just check that the EventId passed are correct
	 * @param submitEvent The SubmitEvent to submit
	 * @return The result of the submit (for submitEvent.eventPlayerList: only the created/updated lines)
	 */
	@Transactional
	public SubmitEventResult submit(SubmitEvent submitEvent) throws Exception {
		// prepare Event
		Event newEvent = submitEvent.getEvent();
		Event existingEvent = get(newEvent.getId());
		if (existingEvent == null) {
			throw new Exception("Event with id: '" + submitEvent.getEvent().getId()
								+ "' does not exist ! Can't update.");
		}

		newEvent.setId(existingEvent.getId());

		// prepare list of EventPlayer (matching on EventId)
		List<EventPlayer> allEventPlayerList = (List<EventPlayer>) eventPlayerService.getAll(); // all Events
		List<EventPlayer> originalEventPlayerList = allEventPlayerList.stream()
																.filter(ep -> ep.getEventId() == newEvent.getId())
																.collect(Collectors.toList()); // only current Event
		List<EventPlayer> newEventPlayerList = submitEvent.getEventPlayerList();

		List<EventPlayer> eventPlayerListToCreate = new ArrayList<>();
		List<EventPlayer> eventPlayerListToUpdate = new ArrayList<>();
		List<EventPlayer> eventPlayerListToDelete = new ArrayList<>();

		// add the EventPlayer to our list to be created or updated
		generateListCreatedUpdated(newEvent, originalEventPlayerList, newEventPlayerList,
									eventPlayerListToCreate, eventPlayerListToUpdate);

		// add the EventPlayer to our list to be deleted
		generateListDeleted(originalEventPlayerList, newEventPlayerList, eventPlayerListToDelete);

		SubmitEventResult submitEventResult =  new SubmitEventResult();
		List<EventPlayer> createdEventPlayerList = new ArrayList<>();
		List<EventPlayer> updatedEventPlayerList = new ArrayList<>();
		List<EventPlayer> deletedEventPlayerList = new ArrayList<>();
		// create part
		//		List<EventPlayer> resultEventPlayerList = eventPlayerService.saveAll(eventPlayerListToSave);
		for (EventPlayer eventPlayer : eventPlayerListToCreate) {
			EventPlayer created = eventPlayerService.create(eventPlayer);
			createdEventPlayerList.add(created);
		}

		// update part
		for (EventPlayer eventPlayer : eventPlayerListToUpdate) {
			EventPlayer updated = eventPlayerService.update(eventPlayer);
			updatedEventPlayerList.add(updated);
		}

		// delete part
//		eventPlayerService.deleteList(eventPlayerListToDelete); // list of EventPlayer
		for (EventPlayer eventPlayer : eventPlayerListToDelete) {
			eventPlayerService.delete(eventPlayer.getId());
			deletedEventPlayerList.add(eventPlayer);
		}

		Event resultEvent = eventRepository.save(newEvent);

		submitEventResult.setEvent(resultEvent);
		submitEventResult.setCreatedEventPlayerList(createdEventPlayerList);
		submitEventResult.setUpdatedEventPlayerList(updatedEventPlayerList);
		submitEventResult.setDeletedEventPlayerList(deletedEventPlayerList);
		submitEventResult.setResultEventPlayerList(newEventPlayerList);


		return submitEventResult;
	}

	/**
	 * Generate the lists "eventPlayerListToCreate" and "eventPlayerListToUpdate"
	 * @param newEvent The Event we want to submit
	 * @param originalEventPlayerList The EventPlayer list before the submit
	 * @param newEventPlayerList The EventPlayer list we are trying to submit
	 * @param eventPlayerListToCreate The output list of EventPlayer to be created
	 * @param eventPlayerListToUpdate The output list of EventPlayer to be updated
	 */
	private void generateListCreatedUpdated(Event newEvent, List<EventPlayer> originalEventPlayerList,
											List<EventPlayer> newEventPlayerList,
											List<EventPlayer> eventPlayerListToCreate,
											List<EventPlayer> eventPlayerListToUpdate) throws Exception {
		for (EventPlayer newEventPlayer : newEventPlayerList) {
			if (newEventPlayer.getEventId() != newEvent.getId()) {
				throw new Exception("One of the passed EventPlayer: '" + newEventPlayer
									+ "' does not have the correct eventId '" + newEventPlayer.getEventId()
									+ "'. Expected: '" + newEvent.getId() + "'. Can't submit.");
			}

			// find if an EventPlayer with that playerId already exists
			EventPlayer eventPlayer = originalEventPlayerList.stream()
														.filter(ep -> ep.getPlayerId() == newEventPlayer.getPlayerId())
														.findAny()
														.orElse(null);

			// if the EventPlayer does not already exist we add it to our list to create
			if (eventPlayer == null) {
				// such an EventPlayer does not already exists, we can add it
				if (playerService.get(newEventPlayer.getPlayerId()) == null ) {
					throw new Exception("newEventPlayer: No Player with id '" + newEventPlayer.getPlayerId()
										+ "' exist ! Can't submit event.");
				}

				EventPlayer eventPlayerToCreate = new EventPlayer(newEvent.getId(),
																newEventPlayer.getPlayerId(),
																newEventPlayer.getPayment());
				eventPlayerListToCreate.add(eventPlayerToCreate);
			}
			else {
				// such an eventPlayer already exists and if the payment value has changed, we update the payment value
				if (newEventPlayer.getPayment() != eventPlayer.getPayment()) {
					EventPlayer eventPlayerToUpdate = (EventPlayer) eventPlayer.clone();
					eventPlayerToUpdate.setPayment(newEventPlayer.getPayment());
					eventPlayerListToUpdate.add(eventPlayerToUpdate);
				}
			}
		}
	}

	/**
	 * Generate the list "eventPlayerListToDelete"
	 * @param originalEventPlayerList The EventPlayer list before the submit
	 * @param newEventPlayerList The EventPlayer list we are trying to submit
	 * @param eventPlayerListToDelete The output list of EventPlayer to be deleted
	 */
	private void generateListDeleted(List<EventPlayer> originalEventPlayerList, List<EventPlayer> newEventPlayerList,
									 List<EventPlayer> eventPlayerListToDelete) {
		for (EventPlayer eventPlayer : originalEventPlayerList) {
			// find if an EventPlayer with that playerId exists in the input of the request
			EventPlayer existingEventPlayer = newEventPlayerList.stream()
					.filter(ep -> ep.getPlayerId() == eventPlayer.getPlayerId())
					.findAny()
					.orElse(null);

			// if no eventPlayersDTO found we add it to our list of EventPlayers to delete
			if (existingEventPlayer == null) {
				eventPlayerListToDelete.add(eventPlayer);
			}
		}
	}

//	private Event getTestEvent() {
//		return new Event(
//				9,
//				"BDM FC A",
//				"5",
//				"2",
//				"Paris Lutece",
//				new Date(System.currentTimeMillis()),
//				7,
//				"Foot a 7",
//				"1"
//			);
//	}

}