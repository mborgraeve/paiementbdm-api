package com.nbarthelemy.paiementbdm.api.services;

import com.nbarthelemy.paiementbdm.api.entities.Season;
import com.nbarthelemy.paiementbdm.api.repository.SeasonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SeasonService {

	/*private List<Event> events;*/

	@Autowired
	private SeasonRepository seasonRepository;

	@Autowired
	public SeasonService() {

	}

	/**
	 * Get all the Season
	 * @return All the Season
	 */
	public Iterable<Season> getAll() {
		return seasonRepository.findAll();
	}

	/**
	 * Get a Season by Id
	 * @param id The Season Id
	 * @return The Season if found, else null
	 */
	public Season get(int id) {
		return seasonRepository.findById(id).orElse(null);
	}

	/***
	 * Create the Season passed in parameter if it does not exist
	 * @param season The Season to be created
	 * @return The created Season
	 * @throws Exception if the Season already exists
	 */
	public Season create(Season season) throws Exception {
		Season existingSeason = get(season.getId());

		if (existingSeason != null) {
			throw new Exception("Season with Id '" + season.getId() + "' already exists ! Can't create.");
		}

		return seasonRepository.save(season);
	}

	/**
	 * Update the Season passed in parameter if it exists
	 * @param season The Season to be updated
	 * @return The updated Season
	 * @throws Exception if the Season does not exists
	 */
	public Season update(Season season) throws Exception {
		if (get(season.getId()) == null) {
			throw new Exception("Season with id: '" + season.getId() + "' does not exist ! Can't update.");
		}

		return seasonRepository.save(season);
	}

//	private Event getTestEvent() {
//		return new Event(
//				9,
//				"BDM FC A",
//				"5",
//				"2",
//				"Paris Lutece",
//				new Date(System.currentTimeMillis()),
//				7,
//				"Foot a 7",
//				"1"
//			);
//	}

}