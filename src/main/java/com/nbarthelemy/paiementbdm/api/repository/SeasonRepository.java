package com.nbarthelemy.paiementbdm.api.repository;

import com.nbarthelemy.paiementbdm.api.entities.Season;
import org.springframework.data.repository.CrudRepository;

//This will be AUTO IMPLEMENTED by Spring into a Bean called eventRepository
//CRUD refers Create, Read, Update, Delete

public interface SeasonRepository extends CrudRepository<Season, Integer> {
	
}
