package com.nbarthelemy.paiementbdm.api.repository;

import org.springframework.data.repository.CrudRepository;

import com.nbarthelemy.paiementbdm.api.entities.EventPlayer;

//This will be AUTO IMPLEMENTED by Spring into a Bean called eventRepository
//CRUD refers Create, Read, Update, Delete

public interface EventPlayerRepository extends CrudRepository<EventPlayer, Integer> {
	
}
