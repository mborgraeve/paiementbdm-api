package com.nbarthelemy.paiementbdm.api.repository;

import org.springframework.data.repository.CrudRepository;

import com.nbarthelemy.paiementbdm.api.entities.Event;

//This will be AUTO IMPLEMENTED by Spring into a Bean called eventRepository
//CRUD refers Create, Read, Update, Delete

public interface EventRepository extends CrudRepository<Event, Integer> {
	
}
