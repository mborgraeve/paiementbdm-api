package com.nbarthelemy.paiementbdm.api.controllers;

import com.nbarthelemy.paiementbdm.api.entities.Season;
import com.nbarthelemy.paiementbdm.api.services.SeasonService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@CrossOrigin
@RequestMapping("/season")
public class SeasonController {

	private SeasonService seasonService;

	public SeasonController(SeasonService seasonService) {
		this.seasonService = seasonService;
	}

    @ResponseBody
    @RequestMapping(value = "all", method= RequestMethod.GET)
    public Iterable<Season> getAll() {
		return this.seasonService.getAll();
	}

	@ResponseBody
	@RequestMapping(value = "/{id}", method= RequestMethod.GET)
	public Season get(@PathVariable("id") int id) {
		return this.seasonService.get(id);
	}

	@ResponseBody
	@RequestMapping(value = "", method= RequestMethod.POST)
	public Season create(@RequestBody Season season) throws Exception {
		return this.seasonService.create(season);
	}

	@ResponseBody
	@RequestMapping(value = "", method= RequestMethod.PUT)
	public Season update(@RequestBody Season season) throws Exception {
		return this.seasonService.update(season);
	}
	
	/*private Event getTestEvent() {
		return new Event(
				5,
				"BDM FC A",
				"5",
				"2",
				"Paris Lutece",
				new Date(System.currentTimeMillis()),
				7,
				"Foot a 7",
				"1"
			);
	}*/
}
