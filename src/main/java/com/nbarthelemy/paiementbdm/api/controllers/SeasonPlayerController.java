package com.nbarthelemy.paiementbdm.api.controllers;

import com.nbarthelemy.paiementbdm.api.entities.SeasonPlayer;
import com.nbarthelemy.paiementbdm.api.services.SeasonPlayerService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@CrossOrigin
@RequestMapping("/seasonplayer")
public class SeasonPlayerController {

	private SeasonPlayerService seasonPlayerService;

	public SeasonPlayerController(SeasonPlayerService seasonPlayerService) {
		this.seasonPlayerService = seasonPlayerService;
	}

    @ResponseBody
    @RequestMapping(value = "all", method= RequestMethod.GET)
    public Iterable<SeasonPlayer> getAll() {
		return this.seasonPlayerService.getAll();
	}

	@ResponseBody
	@RequestMapping(value = "/{id}", method= RequestMethod.GET)
	public SeasonPlayer get(@PathVariable("id") int id) {
		return this.seasonPlayerService.get(id);
	}

	@ResponseBody
	@RequestMapping(value = "/season/{id}", method= RequestMethod.GET)
	public List<SeasonPlayer> getBySeason(@PathVariable("id") int id) {
		return this.seasonPlayerService.getBySeason(id);
	}

	@ResponseBody
	@RequestMapping(value = "/player/{id}", method= RequestMethod.GET)
	public List<SeasonPlayer> getByPlayer(@PathVariable("id") int id) {
		return this.seasonPlayerService.getByPlayer(id);
	}

	@ResponseBody
	@RequestMapping(value = "", method= RequestMethod.POST)
	public SeasonPlayer create(@RequestBody SeasonPlayer seasonPlayer) throws Exception {
		return this.seasonPlayerService.create(seasonPlayer);
	}

    @ResponseBody
    @RequestMapping(value = "", method= RequestMethod.PUT)
    public SeasonPlayer update(@RequestBody SeasonPlayer seasonPlayer) throws Exception {
        return this.seasonPlayerService.update(seasonPlayer);
    }

    @ResponseBody
    @RequestMapping(value = "/{id}", method= RequestMethod.DELETE)
    public void delete(@PathVariable("id") int id) {
        this.seasonPlayerService.delete(id);
    }
	
	/*private Event getTestEvent() {
		return new Event(
				5,
				"BDM FC A",
				"5",
				"2",
				"Paris Lutece",
				new Date(System.currentTimeMillis()),
				7,
				"Foot a 7",
				"1"
			);
	}*/
}
