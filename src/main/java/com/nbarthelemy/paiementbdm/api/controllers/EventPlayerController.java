package com.nbarthelemy.paiementbdm.api.controllers;

import com.nbarthelemy.paiementbdm.api.entities.EventPlayer;
import com.nbarthelemy.paiementbdm.api.services.EventPlayerService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@CrossOrigin
@RequestMapping("/eventplayer")
public class EventPlayerController {
	
	private EventPlayerService eventPlayerService;
	
	public EventPlayerController(EventPlayerService eventPlayerService) {
		this.eventPlayerService = eventPlayerService;
	}

    @ResponseBody
    @RequestMapping(value = "all", method= RequestMethod.GET)
    public Iterable<EventPlayer> getAll() {
		return this.eventPlayerService.getAll();
	}

	@ResponseBody
	@RequestMapping(value = "/{id}", method= RequestMethod.GET)
	public EventPlayer get(@PathVariable("id") int id) {
		return this.eventPlayerService.get(id);
	}

	@ResponseBody
	@RequestMapping(value = "/event/{id}", method= RequestMethod.GET)
	public List<EventPlayer> getByEvent(@PathVariable("id") int id) {
		return this.eventPlayerService.getByEvent(id);
	}

	@ResponseBody
	@RequestMapping(value = "/player/{id}", method= RequestMethod.GET)
	public List<EventPlayer> getByPlayer(@PathVariable("id") int id) {
		return this.eventPlayerService.getByPlayer(id);
	}

	@ResponseBody
	@RequestMapping(value = "", method= RequestMethod.POST)
	public EventPlayer create(@RequestBody EventPlayer eventPlayer) throws Exception {
		return this.eventPlayerService.create(eventPlayer);
	}

    @ResponseBody
    @RequestMapping(value = "", method= RequestMethod.PUT)
    public EventPlayer update(@RequestBody EventPlayer eventPlayer) throws Exception {
        return this.eventPlayerService.update(eventPlayer);
    }

    @ResponseBody
    @RequestMapping(value = "/{id}", method= RequestMethod.DELETE)
    public void delete(@PathVariable("id") int id) {
        this.eventPlayerService.delete(id);
    }
	
	/*private Event getTestEvent() {
		return new Event(
				5,
				"BDM FC A",
				"5",
				"2",
				"Paris Lutece",
				new Date(System.currentTimeMillis()),
				7,
				"Foot a 7",
				"1"
			);
	}*/
}
