package com.nbarthelemy.paiementbdm.api.controllers;

import com.nbarthelemy.paiementbdm.api.dto.SubmitEvent;
import com.nbarthelemy.paiementbdm.api.dto.SubmitEventResult;
import com.nbarthelemy.paiementbdm.api.entities.Event;
import com.nbarthelemy.paiementbdm.api.services.EventService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@CrossOrigin
@RequestMapping("/event")
public class EventController {
	
	private EventService eventService;
	
	public EventController(EventService eventService) {
		this.eventService = eventService;
	}

    @ResponseBody
    @RequestMapping(value = "all", method= RequestMethod.GET)
    public Iterable<Event> getAll() {
		return this.eventService.getAll();
	}

	@ResponseBody
	@RequestMapping(value = "season/{id}", method= RequestMethod.GET)
	public Iterable<Event> getBySeason(@PathVariable("id") int id) {
		return this.eventService.getBySeason(id);
	}

	@ResponseBody
	@RequestMapping(value = "/{id}", method= RequestMethod.GET)
	public Event get(@PathVariable("id") int id) {
		return this.eventService.get(id);
	}

	@ResponseBody
	@RequestMapping(value = "", method= RequestMethod.POST)
	public Event create(@RequestBody Event event) throws Exception {
		return this.eventService.create(event);
	}

	@ResponseBody
	@RequestMapping(value = "bulk", method= RequestMethod.POST)
	public Iterable<Event> create(@RequestBody List<Event> events) throws Exception {
		return this.eventService.bulkCreate(events);
	}

	@ResponseBody
	@RequestMapping(value = "", method= RequestMethod.PUT)
	public Event update(@RequestBody Event event) throws Exception {
		return this.eventService.update(event);
	}

    @ResponseBody
    @RequestMapping(value = "submit", method= RequestMethod.POST)
    public SubmitEventResult submit(@RequestBody SubmitEvent submitEvent) throws Exception {
		return this.eventService.submit(submitEvent);
    }
	
	/*private Event getTestEvent() {
		return new Event(
				5,
				"BDM FC A",
				"5",
				"2",
				"Paris Lutece",
				new Date(System.currentTimeMillis()),
				7,
				"Foot a 7",
				"1"
			);
	}*/
}
