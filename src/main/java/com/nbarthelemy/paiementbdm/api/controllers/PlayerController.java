package com.nbarthelemy.paiementbdm.api.controllers;

//import org.springframework.stereotype.Controller;

import com.nbarthelemy.paiementbdm.api.entities.Player;
import com.nbarthelemy.paiementbdm.api.services.PlayerService;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping("/player")
public class PlayerController {
	
	private PlayerService playerService;
	
	public PlayerController(PlayerService playerService) {
		this.playerService = playerService;
	}

    @ResponseBody
    @RequestMapping(value = "all", method= RequestMethod.GET)
    public Iterable<Player> getAll() {
		return this.playerService.getAll();
	}

	@ResponseBody
	@RequestMapping(value = "season/{id}", method= RequestMethod.GET)
	public Iterable<Player> getBySeason(@PathVariable("id") int id) {
		return this.playerService.getBySeason(id);
	}

	@ResponseBody
	@RequestMapping(value = "/{id}", method= RequestMethod.GET)
	public Player get(@PathVariable("id") int id) {
		return this.playerService.get(id);
	}

    @ResponseBody
    @RequestMapping(value = "", method= RequestMethod.POST)
    public Player create(@RequestBody Player player) throws Exception {
        return this.playerService.create(player);
    }

	@ResponseBody
	@RequestMapping(value = "", method= RequestMethod.PUT)
	public Player update(@RequestBody Player player) throws Exception {
		return this.playerService.update(player);
	}

	@ResponseBody
	@RequestMapping(value = "/{id}", method= RequestMethod.DELETE)
	public void delete(@PathVariable("id") int id) throws Exception {
		throw new Exception("Method forbidden for now !");
//		this.playerService.delete(id);
	}
	
	/*private Player getTestPlayer() {
		return new Player(
				5,
				"BDM FC A",
				"5",
				"2",
				"Paris Lutece",
				new Date(System.currentTimeMillis()),
				7,
				"Foot a 7",
				"1"
			);
	}*/
}
