package com.nbarthelemy.paiementbdm.api.dto;

import com.nbarthelemy.paiementbdm.api.entities.Event;
import com.nbarthelemy.paiementbdm.api.entities.EventPlayer;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class SubmitEventResult {
    private Event event;
    private List<EventPlayer> createdEventPlayerList;
    private List<EventPlayer> updatedEventPlayerList;
    private List<EventPlayer> deletedEventPlayerList;
    private List<EventPlayer> resultEventPlayerList;
}
