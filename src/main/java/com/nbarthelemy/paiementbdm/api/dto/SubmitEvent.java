package com.nbarthelemy.paiementbdm.api.dto;

import com.nbarthelemy.paiementbdm.api.entities.Event;
import com.nbarthelemy.paiementbdm.api.entities.EventPlayer;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class SubmitEvent {
    private Event event;
    private List<EventPlayer> eventPlayerList;
}
