
-- Table: Events
CREATE TABLE paiement_bdm.Events (
    Id INTEGER PRIMARY KEY UNIQUE,
    HomeTeam VARCHAR(128),
    HomeScore VARCHAR(4),
    VisitorScore VARCHAR(4),
    VisitorTeam VARCHAR(128),
    MatchDate DATE,
    MatchDay INTEGER,
    PlayCategory VARCHAR(32),
    Division VARCHAR(32)
    );

-- Table: Players
CREATE TABLE paiement_bdm.Players (
    Id INTEGER NOT NULL AUTO_INCREMENT UNIQUE,
    BdmId INTEGER PRIMARY KEY,
    Firstname VARCHAR(64),
    Lastname VARCHAR(64),
    JerseyNumber INTEGER
    );

-- Table: EventPlayers
CREATE TABLE paiement_bdm.EventPlayers (
    Id INTEGER NOT NULL AUTO_INCREMENT UNIQUE,
    EventId INTEGER,
    PlayerId INTEGER,
    Payment FLOAT
    );
